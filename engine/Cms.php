<?php

namespace Engine;

use Engine\Helper\Common;

class Cms
{

    private $di;
    public $router;

    /**
     * cms constructor.
     * @param $di
     */
    public function __construct($di)
    {
        $this->di = $di;
        $this->router = $this->di->get('router');
    }

    /**
     * Run cms
     */
    public function run()
    {
        $this->router->add('home','/','HomeController:index');
        $this->router->add('product','/product/{id}','ProductController:index');
        //d($this->router);
        $routerDispatch = $this->router->dispatch(Common::getMethod(),Common::getPathUrl());
        dd($routerDispatch);
    }


}