<?php

namespace Engine\Core\Database;

use PDO;

class Connection
{
    private $link;

    public function __construct()
    {
        $this->connect();
    }

    /**
     * @return $this
     */
    private function connect()
    {
        $config = [
            'host' => 'localhost',
            'dbname'=>'proj',
            'charset'=>'utf8',
            'username'=>'admin',
            'password'=>'123',
        ];
        $dsn = 'mysql:host='.$config['host'].';dbname='.$config['dbname'].';charset='.$config['charset'];
        $this->link = new PDO($dsn,$config['username'],$config['password']);
        return $this;
    }

    /**
     * @param $sql
     * @return mixed
     */
    public function execute($sql)
    {
        $stn = $this->link->prepare($sql);
        return $stn->execute();
    }

    /**
     * @param $sql
     * @return array
     */
    public function query($sql)
    {
        $stn = $this->link->prepare($sql);
        $stn->execute();
        $res = $stn->fetchAll(PDO::FETCH_ASSOC);

        return $res === false ? [] : $res;
    }

}