<?php

namespace Engine\Core\Router;

class Router
{
    private $routes = [];
    private $host;
    private $dispather;

    public function __construct($host)
    {
        $this->host = $host;
    }

    /**
     * @param $key
     * @param $pattern
     * @param $controller
     * @param string $method
     */
    public function add($key, $pattern, $controller, $method = 'GET')
    {
        $this->routes[$key] = [
            'pattern'       => $pattern,
            'controller'    => $controller,
            'method'        => $method,
        ];
    }

    public function dispatch($method, $uri)
    {
        return $this->getDispatcher()->dispatch($method,$uri);
    }

    public function getDispatcher()
    {
        if ($this->dispather == null) {
            $this->dispather = new UrlDispather();
            foreach ($this->routes as $route) {
                $this->dispather->register($route['method'],$route['pattern'],$route['controller']);
            }
        }
        return $this->dispather;
    }

}