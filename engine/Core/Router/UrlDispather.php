<?php


namespace Engine\Core\Router;


class UrlDispather
{
    private $methods = [
        'GET',
        'POST',
    ];

    private $routes = [
        'GET' => [],
        'POST' => []
    ];

    private $patterns = [
        'int' => '[0-9]+',
        'str' => '[a-zA-z\.\-_%]+',
        'any' => '[a-zA-z0-9\.\-_%]+'
    ];

    /**
     * @param $key
     * @param $pattern
     */
    public function addPattern($key, $pattern)
    {
        $this->patterns[$key] = $pattern;
    }

    /**
     * @param $method
     * @return array|mixed
     */
    private function routes($method)
    {
        dd($this->routes);
        return isset($this->routes[$method]) ? $this->routes['$method'] : [];
    }

    /**
     * @param $method
     * @param $url
     * @return DispatchedRoute
     */
    public function dispatch($method, $url)
    {
        $routes = $this->routes(strtoupper($method));
        if (array_key_exists($url, $routes)) {
            return new DispatchedRoute($routes[$url]);
        }

        return $this->doDispatch($method, $url);

    }

    private function doDispatch($method, $url)
    {
        foreach ($this->routes(strtoupper($method)) as $route => $controller) {
            $pattern = '#^' . $route . '$#s';
            if (preg_match($pattern,$url,$parameters)) {
                return new DispatchedRoute($controller,$parameters);
            }

        }
    }

    public function register($method, $pattern, $controller)
    {
        $this->routes[strtoupper($method)][$pattern] = $controller;
    }
}