<?php

namespace Engine\DI;

class DI
{
    private $container = [];

    /**
     * @param $key
     * @param $val
     * @return $this
     */
    public function set($key, $val)
    {
        $this->container[$key] = $val;
        return $this;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->container[$key];
    }

    /**
     * @param $key
     * @return bool
     */
    public function has($key)
    {
        return isset($this->container[$key]) ? $this->container[$key] : null;
    }


}