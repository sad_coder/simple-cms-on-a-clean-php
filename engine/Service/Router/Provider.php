<?php

namespace Engine\Service\Router;

use Engine\Service\AbstractProvider;
use Engine\Core\Router\Router;

class Provider extends AbstractProvider
{
    public $serviceName = 'router';


    public function init()
    {
        $router = new Router('http://proj.loc/');
        $this->serviceName = 'router';
        $this->di->set($this->serviceName,$router);
    }
}